"Better copy and paste
set pastetoggle=<F2>
set clipboard=unamed

" Mouse and backspace
set bs=2
set mouse=a

" bind Leader key
let mapleader=" "

" Remove highlight from your last search
noremap <C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>

" bind rotate window
map <c-r> <c-w>r
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" easier moving between tabs
map <Leader>h <esc>:tabprevious<CR>
map <Leader>l <esc>:tabnext<CR>

" easier moving between buffers
map <Leader>j :bnext<CR>
map <Leader>k :bprev<CR>

" New line without insert
nnoremap <C-CR> i<CR><Esc>

" Indentation
set expandtab " replace tabs by white space characters
set shiftwidth=4 " set tabulations to be 4 white spaces
vnoremap < <gv
noremap > >gv

" Enable syntax highlighting
filetype off
filetype plugin indent on
syntax on

" Showing line numbers and length
set number      " show line numbers
set tw=79       " width of document
set nowrap      " don't wrap on load
set fo-=t       " don't wrap text when typing
set colorcolumn=80
highlight ColorColumn ctermbg=232

" Useful settings
set hidden
set history=700
set undolevels=700
set wildmenu
set wildmode=list,full
set path+=**
set nocompatible
set wildignorecase
set splitbelow
set splitright
set wildignore+=*.pycharm
set wildignore+=*build/*
set wildignore+=*/coverage/*
set wildignore+=*/tmp/*,*.so,*.swp,*.zip     " MacOSX/Linux
" avoid double forward slash when tabbing
cnoremap <expr> / wildmenumode() ? "\<C-E>" : "/"
" full path shortcut
cnoreabbr <expr> %% fnameescape(expand('%:p'))

" Use spaces instead of tabs
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab

" Setup Pathogen to manage your plugins
call pathogen#infect()
syntax on
filetype plugin indent on

" Settings for vim-powerline
set laststatus=2
set t_Co=256
"colo elflord

" Settings for ctrlp
let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$',
  \ 'file': '\v\.(exe|so|dll)$',
  \ 'link': 'some_bad_symbolic_links',
  \ }
let g:ctrlp_working_path_mode = 'ra'

" Settings for nerdtree
nnoremap <Leader>n :NERDTreeFocus<CR>

" Settings for python folding
set nofoldenable

" Auto open quickfix after running :make :grep and :lvmgrep
augroup myvimrc
    autocmd!
    autocmd QuickFixCmdPost [^l]* cwindow
    autocmd QuickFixCmdPost l*    lwindow
augroup END

" Airline
" Integrating with powerline fonts
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline_theme='supernova'
